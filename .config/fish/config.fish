echo \e"[3"(math (random) % 6 + 1)"mB)"

export EDITOR=nvim
export SDL_SOUNDFONTS=$HOME/Documents/MIDI\ Soundfonts/GM_Roland.sf2
export VISUAL=

# ALIASES
alias ls='exa -al --group-directories-first --icons'
alias tree='exa -al --group-directories-first --icons --tree'
alias bat='bat --theme Dracula'
alias emacs='emacsclient -c -a "emacs"'
alias em='emacsclient -t -a ""'
alias q='exit'

# VI MODE
fish_vi_key_bindings

# FUNCTIONS

# !! (from https://github.com/oh-my-fish/plugin-bang-bang)
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end
function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
bind ! __history_previous_command
bind '$' __history_previous_command_arguments
if test "$fish_key_bindings" = 'fish_vi_key_bindings'
  bind --mode insert ! __history_previous_command
  bind --mode insert '$' __history_previous_command_arguments
end

# make and change directory
function mkcd --argument dirname
  command mkdir -p $dirname && cd $dirname
end

# print color table
function colors
for i in (seq 0 7)
    printf '\e[3%sm' "$i"
    for j in (seq 0 7)
      printf '\e[4%sm * ' "$j"
    end
    printf '\n'
  end
  printf '\e[0m'  
end



# DRACULA THEME
set -l foreground f8f8f2
set -l selection 44475a
set -l comment 6272a4
set -l red ff5555
set -l orange ffb86c
set -l yellow f1fa8c
set -l green 50fa7b
set -l purple bd93f9
set -l cyan 8be9fd
set -l pink ff79c6

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment



# STARSHIP
starship init fish | source
