local use = require('packer').use

-- PLUGINS

require('packer').startup(function()
  use 'wbthomason/packer.nvim'
  use 'iamcco/markdown-preview.nvim'
  use {'neoclide/coc.nvim',
       branch = 'release'}
  use {'dracula/vim',
       as = 'dracula'}
  use {'nvim-lualine/lualine.nvim',
       requires = {'kyazdani42/nvim-web-devicons', opt = true}}
  use 'ap/vim-css-color'
  use 'terryma/vim-multiple-cursors'
  use 'tpope/vim-commentary'
  use 'preservim/nerdtree'
  use 'tpope/vim-surround'
  use 'dcampos/nvim-snippy'
end)

