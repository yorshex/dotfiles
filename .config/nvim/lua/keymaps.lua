local g = vim.g
local map = vim.api.nvim_set_keymap

local opts = {
  default = {noremap = true,  silent = true},
  n =       {noremap = false, silent = false},
  nore =    {noremap = true,  silent = false},
  si =      {noremap = false, silent = true},
  noresi =  {noremap = true,  silent = true}
}

-- KEYMAPS

g.mapleader = ' '

map('', '<leader><leader>', '<Nop>', opts.default)

map('n', '<Esc>', ':nohl<CR>', opts.default)

map('n', '<leader>tw', ':set wrap!<CR>:set wrap?<CR>', opts.default)
map('n', '<leader>tl', ':set list!<CR>:set list?<CR>', opts.default)
map('n', '<leader>te', ':set expandtab!<CR>:set expandtab?<CR>', opts.default)
map('n', '<leader>tn', ':set number!<CR>:set number?<CR>', opts.default)
map('n', '<leader>tr', ':set relativenumber!<CR>:set relativenumber?<CR>', opts.default)

map('n', '<leader>.', ':NERDTreeToggle<CR>', opts.default)
map('n', '<leader>*', ':CocDiagnostics<CR>', opts.default)

map('n', '<leader>h', '<C-f>', opts.default)
map('n', '<leader>j', '<C-d>', opts.default)
map('n', '<leader>k', '<C-u>', opts.default)
map('n', '<leader>l', '<C-b>', opts.default)

map('i', '<S-Tab>', '<C-v><Tab>', opts.default);
map('i', '<M-h>', '<Left>', opts.default)
map('i', '<M-j>', '<Down>', opts.default)
map('i', '<M-k>', '<Up>', opts.default)
map('i', '<M-l>', '<Right>', opts.default)

map('i', '<M-Space>', '\xA0', opts.default)

require('snippy').setup({
  mappings = {
    is = {
      ['<C-l>'] = 'expand_or_advance',
      ['<C-k>'] = 'previous'
    },
  }
})
