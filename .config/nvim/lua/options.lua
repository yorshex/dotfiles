local g = vim.g
local o = vim.o

-- OPTIONS

-- Working clipboard
o.clipboard = 'unnamedplus'
-- Improved expirience B)
o.compatible = false

-- Line number
o.number = true
o.relativenumber = true
o.cursorline = true
o.cursorlineopt = 'number'

-- Search options
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
o.smartcase = true

-- Indenting
o.expandtab = true
o.shiftround = true
o.autoindent = true
o.smarttab = true
o.tabstop = 8
o.shiftwidth = 2

-- Whitespace visibility
o.listchars = 'space:·,nbsp:_,tab:――⟶,extends:→,precedes:←'
-- in kitty terminal with jb mono tab looks cool, okay?
o.list = true

-- Lualine

require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '·', right = '·'},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
    globalstatus = false,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {}
}

-- Misc
o.mouse='a'
o.wrap = false
o.encoding = "utf-8"
o.scrolloff = 5
o.sidescrolloff = 10
o.wildmenu = true
o.swapfile = false
o.history = 1000
o.termguicolors = true
o.timeout = true
o.timeoutlen = 5000
g.NERDTreeShowHidden = 1
o.colorcolumn = '80'
vim.cmd('colorscheme dracula')
