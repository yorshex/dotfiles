" Improved expirience B)
set nocompatible



" PLUGINS
" call plug#begin()
"   Plug 'dracula/vim', {'as': 'dracula'}
"   Plug 'ap/vim-css-color', {'as': 'css-color'}
"   Plug 'terryma/vim-multiple-cursors', {'as': 'multiple-cursors'}
"   Plug 'tpope/vim-commentary', {'as': 'commentary'}
"   Plug 'preservim/nerdtree'
"   Plug 'tpope/vim-surround', {'as': 'surround'}
" call plug#end()



" OPTIONS

" Idk what it means, but without this clipboard not working
set clipboard=unnamedplus

" Line numbers
set number
set relativenumber
set cursorline
set cursorlineopt=number

" Search
set hlsearch
set incsearch
set ignorecase
set smartcase

" Tabs
set autoindent
set expandtab
set tabstop=2
set shiftround
set shiftwidth=2
set smarttab

" Whitespace
set listchars=space:·,nbsp:_,tab:\ \ >,extends:→,precedes:←
set list

" Misc
set nowrap
set encoding=utf-8
set scrolloff=5
set sidescrolloff=10
set wildmenu " Tab completion in command mode
set noswapfile
set history=1000 " Undo history length
set termguicolors
set mouse=a
set statusline=(%n)\ %F%h%m%r%=%y%{\"[\".(&enc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\"}%{\"[\".(&fileformat).\"]\"}\ (%l:%c)\ %P
let NERDTreeShowHidden=1
syntax on



" KEYBINDINGS

set notimeout
nnoremap <Space> <Nop>

" Toggle Options
nnoremap <Space>tw :set wrap!<CR>:set wrap?<CR>
nnoremap <Space>tl :set list!<CR>:set list?<CR>
nnoremap <Space>te :set expandtab!<CR>:set expandtab?<CR>
nnoremap <Space>tn :set number!<CR>:set number?<CR>
nnoremap <Space>tr :set relativenumber!<CR>:set relativenumber?<CR>

" Open NERDTree
nnoremap <Space>. :NERDTreeToggle<CR>

" Windows
nnoremap <Space>w <C-w>

" Movement
nnoremap <Space>h <C-f>
nnoremap <Space>j <C-d>
nnoremap <Space>k <C-u>
nnoremap <Space>l <C-b>

" Cancel
nnoremap <Space><Space> <Nop>



" colorscheme dracula
