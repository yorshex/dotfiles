config.load_autoconfig(False)

# SETTINGS
c.auto_save.session = True
c.completion.shrink = True
c.tabs.favicons.scale = 1
c.tabs.indicator.width = 2
c.tabs.indicator.padding = {"bottom": 4, "left": 0, "right": 5, "top": 4}
c.tabs.title.format = '{audio}{index}: {current_title}'
c.tabs.title.format_pinned = '{index}'
c.statusbar.widgets = ["keypress", "url", "progress"]
c.statusbar.padding = {"bottom": 2, "left": 8, "right": 8, "top": 2}
c.tabs.padding = {"bottom": 2, "left": 8, "right": 8, "top": 2}
c.completion.scrollbar.padding = 4
c.scrolling.smooth = True;
c.tabs.show = 'always'
c.statusbar.show = 'always'
c.content.autoplay = False
c.url.searchengines = {
    "DEFAULT": "https://duckduckgo.com/?q={}",
    "duck": "https://duckduckgo.com/?q={}",
    "trans": "https://translate.google.com/?text={}&op=translate",
    "hints": "https://devhints.io/?q={}",
    "yt": "https://www.youtube.com/results?search_query={}"
}

# FONTS
c.fonts.default_family = 'JetBrains Mono NL'
c.fonts.default_size = '10pt'
c.fonts.contextmenu = 'default_size default_family'
# c.fonts.web.family.standard = 'Noto Sans'
# c.fonts.web.family.sans_serif = 'Noto Sans'
# c.fonts.web.family.fixed = 'Noto Sans Mono'
# c.fonts.web.family.serif = 'Noto Serif'
# c.fonts.web.family.cursive = 'Noto Serif'
# c.fonts.web.family.fantasy = 'Iosevka Fixed'

# COLORS
# Dracula theme palette
palette = {
    'bg': '#282a36',
    'bg-dark': '#21222c',
    'bg-bright': '#44475a',
    'fg': '#f8f8f2',
    'fg-dark': '#e0e0e0',
    'fg-bright': '#ffffff',
    'hl': '#6272a4',
    'cyan': '#8be9fd',
    'green': '#50fa7b',
    'orange': '#ffb86c',
    'pink': '#ff79c6',
    'purple': '#bd93f9',
    'red': '#ff5555',
    'yellow': '#f1fa8c'
}

# Status bar
c.colors.statusbar.normal.bg = palette['bg']
c.colors.statusbar.normal.fg = palette['fg']
c.colors.statusbar.insert.bg = palette['bg-dark']
c.colors.statusbar.insert.fg = palette['fg-bright']
c.colors.statusbar.command.bg = palette['bg']
c.colors.statusbar.command.fg = palette['pink']
c.colors.statusbar.caret.bg = palette['bg']
c.colors.statusbar.caret.fg = palette['purple']
c.colors.statusbar.caret.selection.bg = palette['bg']
c.colors.statusbar.caret.selection.fg = palette['purple']
c.colors.statusbar.passthrough.bg = palette['bg']
c.colors.statusbar.passthrough.fg = palette['orange']
c.colors.statusbar.progress.bg = palette['purple']
c.colors.statusbar.url.fg = palette['fg']
c.colors.statusbar.url.success.https.fg = palette['fg']
c.colors.statusbar.url.success.http.fg = palette['fg-dark']
c.colors.statusbar.url.hover.fg = palette['cyan']
c.colors.statusbar.url.warn.fg = palette['orange']
c.colors.statusbar.url.error.fg = palette['red']
c.colors.statusbar.private.bg = palette['bg-bright']
c.colors.statusbar.private.fg = palette['fg']
c.colors.statusbar.command.private.bg = palette['bg-bright']
c.colors.statusbar.command.private.fg = palette['pink']

# Messages
c.colors.messages.info.bg = palette['bg']
c.colors.messages.info.fg = palette['hl']
c.colors.messages.info.border = palette['bg']
c.colors.messages.warning.bg = palette['bg']
c.colors.messages.warning.fg = palette['orange']
c.colors.messages.warning.border = palette['bg']
c.colors.messages.error.bg = palette['red']
c.colors.messages.error.fg = palette['fg-bright']
c.colors.messages.error.border = palette['red']

# Hints and keyhints
c.colors.hints.bg = palette['bg']
c.colors.hints.fg = palette['purple']
c.colors.hints.match.fg = palette['fg']
c.colors.keyhint.bg = palette['bg']
c.colors.keyhint.fg = palette['purple']
c.colors.keyhint.suffix.fg = palette['fg']
c.hints.border = 'none'

# Prompts
c.colors.prompts.bg = palette['bg']
c.colors.prompts.fg = palette['fg']
c.colors.prompts.selected.bg = palette['bg-bright']
c.colors.prompts.selected.fg = palette['fg-bright']
c.colors.prompts.border = 'none'

# Tab bar
c.colors.tabs.bar.bg = palette['bg']
c.colors.tabs.odd.bg = palette['bg-bright']
c.colors.tabs.odd.fg = palette['fg']
c.colors.tabs.even.bg = palette['bg-bright']
c.colors.tabs.even.fg = palette['fg']
c.colors.tabs.selected.odd.bg = palette['bg']
c.colors.tabs.selected.odd.fg = palette['fg']
c.colors.tabs.selected.even.bg = palette['bg']
c.colors.tabs.selected.even.fg = palette['fg']
c.colors.tabs.pinned.odd.bg = palette['bg-bright']
c.colors.tabs.pinned.odd.fg = palette['fg']
c.colors.tabs.pinned.even.bg = palette['bg-bright']
c.colors.tabs.pinned.even.fg = palette['fg']
c.colors.tabs.pinned.selected.odd.bg = palette['bg']
c.colors.tabs.pinned.selected.odd.fg = palette['fg']
c.colors.tabs.pinned.selected.even.bg = palette['bg']
c.colors.tabs.pinned.selected.even.fg = palette['fg']
c.colors.tabs.indicator.start = palette['purple']
c.colors.tabs.indicator.stop = palette['green']
c.colors.tabs.indicator.error = palette['red']
c.colors.tabs.indicator.system = 'none'

# Downloads
c.colors.downloads.bar.bg = palette['bg']
c.colors.downloads.start.bg = palette['bg']
c.colors.downloads.start.fg = palette['fg']
c.colors.downloads.stop.bg = palette['bg']
c.colors.downloads.stop.fg = palette['green']
c.colors.downloads.error.bg = palette['bg']
c.colors.downloads.error.fg = palette['red']
c.colors.downloads.system.bg = 'none'
c.colors.downloads.system.fg = 'none'

# Completion
c.colors.completion.category.bg = palette['bg']
c.colors.completion.category.fg = palette['purple']
c.colors.completion.category.border.top = palette['bg']
c.colors.completion.category.border.bottom = palette['bg']
c.colors.completion.odd.bg = palette['bg']
c.colors.completion.even.bg = palette['bg']
c.colors.completion.fg = palette['fg']
c.colors.completion.match.fg = palette['orange']
c.colors.completion.item.selected.bg = palette['bg-bright']
c.colors.completion.item.selected.border.top = palette['bg-bright']
c.colors.completion.item.selected.border.bottom = palette['bg-bright']
c.colors.completion.item.selected.fg = palette['fg-bright']
c.colors.completion.item.selected.match.fg = palette['orange']
c.colors.completion.scrollbar.bg = palette['bg']
c.colors.completion.scrollbar.fg = palette['hl']

# Context menu
c.colors.contextmenu.menu.bg = palette['bg']
c.colors.contextmenu.menu.fg = palette['fg']
c.colors.contextmenu.disabled.bg = palette['bg']
c.colors.contextmenu.disabled.fg = palette['hl']
c.colors.contextmenu.selected.bg = palette['bg-bright']
c.colors.contextmenu.selected.fg = palette['fg-bright']

config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')
