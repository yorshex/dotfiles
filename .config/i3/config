set $exe exec --no-startup-id

# Autostart for some programs
$exe launch_polybar
$exe picom
$exe nm-applet
$exe dunst
$exe nitrogen --restore
#$exe /bin/emacs --daemon
$exe flameshot
$exe xset s off -dpms

# My terminal
set $term kitty

# Workspaces naming
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# Window rules
for_window [class="Qalculate-gtk"] floating enable
for_window [class="Pavucontrol"] floating enable
for_window [class="copyq"] floating enable

# CONTROL

# Mod key
set $mod Mod4

# Uncategorized
bindsym $mod+Return $exe $term
bindsym $mod+d $exe dmenu_run_history
bindsym $mod+Tab $exe rofi -modi window -show window
bindsym $mod+grave $exe rofi -modi emoji -show emoji
bindsym Print $exe flameshot gui
bindsym Shift+Print $exe flameshot full
bindsym $mod+Shift+q kill

# Mouse on window don't focuses this window 
focus_follows_mouse off

# Mouse+Mod can move and resize windows
floating_modifier $mod

# Focusing
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# Moving windows
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# Modifying layout
bindsym $mod+slash split h
bindsym $mod+backslash split v

bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Fullscreen
bindsym $mod+f fullscreen toggle

# Focus floating
bindsym $mod+space focus mode_toggle
# Move window to floating
bindsym $mod+Shift+space floating toggle

# focus parent and child object in tree
bindsym $mod+a focus parent
bindsym $mod+Shift+a focus child

# Focusing workspaces
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# Sending windows to workspaces
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# Reload config
bindsym $mod+Shift+r reload
# Restart
bindsym $mod+Mod1+r restart
# Exit from i3
bindsym $mod+Shift+e $exe dmenu-i3-exit

# Dunst
bindsym $mod+Ctrl+Left $exe dunstctl context
bindsym $mod+Ctrl+Down $exe dunstctl history-pop
bindsym $mod+Ctrl+Up $exe dunstctl close
bindsym $mod+Ctrl+Right $exe dunstctl close-all

bindsym $mod+Ctrl+h $exe dunstctl context
bindsym $mod+Ctrl+j $exe dunstctl history-pop
bindsym $mod+Ctrl+k $exe dunstctl close
bindsym $mod+Ctrl+l $exe dunstctl close-all

# Scratchpad
bindsym $mod+z scratchpad show
bindsym $mod+Shift+z move scratchpad

# Resizing
bindsym $mod+Mod1+Left resize shrink width 10 px or 10 ppt
bindsym $mod+Mod1+Down resize grow height 10 px or 10 ppt
bindsym $mod+Mod1+Up resize shrink height 10 px or 10 ppt
bindsym $mod+Mod1+Right resize grow width 10 px or 10 ppt

bindsym $mod+Mod1+h resize shrink width 10 px or 10 ppt
bindsym $mod+Mod1+j resize grow height 10 px or 10 ppt
bindsym $mod+Mod1+k resize shrink height 10 px or 10 ppt
bindsym $mod+Mod1+l resize grow width 10 px or 10 ppt

# APPEARANCE

# Font
font pango:JetBrains Mono NL 10

# No titles, 2 pixel border
for_window [class=.*] border pixel 2

# class                 border  bg      text    indictr child_border
client.focused          #6272A4 #6272A4 #F8F8F2 #6272A4 #6272A4
client.focused_inactive #44475A #44475A #F8F8F2 #44475A #44475A
client.unfocused        #282A36 #282A36 #BFBFBF #282A36 #282A36
client.urgent           #FF5555 #FF5555 #F8F8F2 #FF5555 #FF5555
client.placeholder      #282A36 #282A36 #F8F8F2 #282A36 #282A36

client.background       #F8F8F2
