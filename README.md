## Configs and files

- Fish shell `.config/fish/config.fish` - requires [starship prompt](https://starship.rs/).
- Starship prompt `.config/starship.toml`
- GNU Emacs `.emacs.d/init.el`
- Dunst notification-daemon `.config/dunst/dunstrc`
- i3 WM `.config/i3/config` - requires all dmenu scripts in `.local/bin` and [`rofi-emoji`](https://github.com/Mange/rofi-emoji).
- i3blocks `.config/i3blocks/config`
- Kitty terminal `.config/kitty/kitty.conf`
- Qutebrowser `.config/qutebrowser/config.py`
- Rofi `.config/rofi/config.rasi`
- NVim `.config/nvim/init.lua` and `.config/nvim/lua` - requires packer.
- Polybar `.config/polybar/config`

### .local/bin

- `dmenu-i3-exit` - simple [dmenu](https://tools.suckless.org/dmenu/) script to shutdown, reboot or exit from i3.
- [`dmenu_run_history`](https://tools.suckless.org/dmenu/scripts/dmenu_run_with_command_history/) - `dmenu_run` with history.

## Links

- [JetBrains Mono](https://www.jetbrains.com/mono/) - font I use everywhere.
- [Dracula theme](https://draculatheme.com/) - theme I use everywhere.
