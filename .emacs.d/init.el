;; * PACKAGE MANAGEMENT

;; ** Melpa

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; ** use-package.el

(unless (package-installed-p 'use-package)
  (package-install 'use-package))



;; * STARTUP
;; - This section fully stolen from https://gitlab.com/dwt1/dotfiles/-/tree/master/.emacs.d.gnu

;; ** Garbage collection

(use-package gcmh
  :ensure t
  :config
  (gcmh-mode 1)
  (setq gc-cons-threshold 402653184
        gc-cons-percentage 0.6))

;; ** Start massage

(add-hook 'emacs-startup-hook
          (lambda ()
	    (message "Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds" (float-time (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; ** Silence compiler warnings

(if (boundp 'comp-deferred-compilation)
  (setq comp-deferred-compilation nil)
  (setq native-comp-deferred-compilation nil))
(setq load-prefer-newer noninteractive)



;; * EMACS

;; ** Uncategorized settings

(setq inhibit-startup-screen t)
(setq initial-scratch-message nil)
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq custom-file "~/.emacs.d/custom.el")
(setq ring-bell-function 'ignore)
(setq frame-title-format '("%b - Emacs"))
(load custom-file)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(column-number-mode 1)
(set-default 'truncate-lines t)


;; ** Smooth scrolling

(setq mouse-wheel-scroll-amount '(2 ((shift) . 5)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse t)
(setq scroll-step 1)
(setq scroll-margin 7)



;; * KEYBINDINGS

(use-package general
   :ensure t
   :config
   (general-evil-setup t))

(global-set-key (kbd "C-=")            'text-scale-increase)
(global-set-key (kbd "C--")            'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>")   'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
(global-set-key (kbd "C-x S-k")        'kill-current-buffer)

(nvmap :prefix "SPC"
  "SPC" '(smex      :which-key "M-x")
  "."   '(find-file :which-key "Find file")

  "w"   '(:which-key "Window prefix")
  "w h" '(evil-window-left       :which-key "Left")
  "w j" '(evil-window-down       :which-key "Down")
  "w k" '(evil-window-up         :which-key "Up")
  "w l" '(evil-window-right      :which-key "Right")
  "w H" '(evil-window-move-left  :which-key "Move left")
  "w J" '(evil-window-move-down  :which-key "Move down")
  "w K" '(evil-window-move-up    :which-key "Move up")
  "w L" '(evil-window-move-right :which-key "Move right")

  "f"   '(:which-key "File prefix"))



;; * EVIL

(use-package undo-tree
  :ensure t
  :config
  (global-undo-tree-mode 1))
(use-package evil
  :after undo-tree
  :ensure t
  :init
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-tree)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  :config
  (evil-mode 1))
; (use-package evil-collection
;   :after evil
;   :ensure t
;   :config
;   (evil-collection-init))



;; * FLYCHECK

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode t))



;; * DRAG STUFF

(use-package drag-stuff
  :ensure t
  :config
  (global-set-key (kbd "M-<left>") 'drag-stuff-left)
  (global-set-key (kbd "M-<down>") 'drag-stuff-down)
  (global-set-key (kbd "M-<up>") 'drag-stuff-up)
  (global-set-key (kbd "M-<right>") 'drag-stuff-right)
  (nvmap
    "M-h"  'drag-stuff-left
    "M-j"  'drag-stuff-down
    "M-k"  'drag-stuff-up
    "M-l"  'drag-stuff-right))



;; * IDO

(use-package ido
  :config
  (setq ido-everywhere t)
  (ido-mode t))
(use-package smex
  :ensure t
  :config
  (global-set-key (kbd "M-x")     'smex)
  (global-set-key (kbd "C-x M-x") 'smex-major-mode-commands))
(use-package ido-grid-mode
  :ensure t
  :config
  (ido-grid-mode 1))



;; * FIXMEE

(use-package fixmee
  :ensure t
  :init
  (use-package button-lock)
  :config
  (global-fixmee-mode 1))



;; * WHICH KEY

(use-package which-key
  :ensure t
  :config
  (which-key-mode 1))



;; * LIGATURE
; (use-package ligature
;   :load-path "~/.emacs.d/ligature"
;   :config
;   ;; Enable the "www" ligature in every possible major mode
;   (ligature-set-ligatures 't '("www"))
;   ;; Enable traditional ligature support in eww-mode, if the
;   ;; `variable-pitch' face supports it
;   (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
;   ;; Enable all Cascadia Code ligatures in programming modes
;   (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
;                                        ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
;                                        "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
;                                        "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
;                                        "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
;                                        "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
;                                        "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
;                                        "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
;                                        ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
;                                        "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
;                                        "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
;                                        "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
;                                        "\\\\" "://"))
;   ;; Enables ligature checks globally in all buffers. You can also do it
;   ;; per mode with `ligature-mode'.
;   (global-ligature-mode t))



;; * THEME

;; ** Relative line numbers

(use-package display-line-numbers
  :config
  (setq display-line-numbers-type 'relative)
  (global-display-line-numbers-mode 1))

;; ** Dracula theme

(use-package dracula-theme
  :ensure t
  :config
  (load-theme 'dracula))

;; ** Cursor colors for dracula theme

(setq evil-normal-state-cursor   '("#f1fa8c" box)
      evil-insert-state-cursor   '("#f1fa8c" (bar . 2))
      evil-visual-state-cursor   '("#ff79c6" (hbar . 3))
      evil-operator-state-cursor '("#50fa7b" (hbar . 3))
      evil-replace-state-cursor  '("#f1fa8c" (hbar . 3))
      evil-motion-state-cursor   '("#ffb86c" box)
      evil-emacs-state-cursor    '("#f8f8f2" box))

;; ** Faces

(custom-set-faces
 '(default                  ((t (:family "Hack" :height 160 :weight medium) )))
 '(fixed-pitch              ((t (:family "Hack" :height 160 :weight medium) )))
 '(fixed-pitch-serif        ((t (:family "Hack" :height 160 :weight medium) )))
 '(variable-pitch           ((t (:family "Source Sans" :height 160 :weight medium) )))
 ;; - Defaults

 '(line-number-current-line ((t (:inherit line-number :foreground "#f1fa8c") )))
 ;; - Current line number

 '(custom-button            ((t (:background "#44475a" :foreground "#f8f8f2" :box (:line-width -1 :style released-button)) )))
 '(custom-button-mouse      ((t (:background "#6272a4" :foreground "#f8f8f2" :box (:line-width -1 :style released-button)) )))
 '(custom-button-pressed    ((t (:background "#282a36" :foreground "#f8f8f2" :box (:line-width -1 :style pressed-button)) )))
 ;; - Custom button faces

 '(flycheck-info            ((t (:underline (:color "#50fa7b" :style wave)) )))
 '(flycheck-warning         ((t (:underline (:color "#ffb86c" :style wave)) )))
 '(flycheck-error           ((t (:underline (:color "#ff5555" :style wave)) )))
 ;; - Flycheck faces
 )
